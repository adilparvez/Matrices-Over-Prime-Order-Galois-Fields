import java.util.ArrayList;


public class MatrixOverGF {
	
	private final int numRows;
	private final int numCols;
	private final int prime;
	
	private int[][] matrix;
	private int[] inverses;
	
	MatrixOverGF(int numRows, int numCols, int prime) {
		this.numRows = numRows;
		this.numCols = numCols;
		this.prime = prime;
		this.matrix = new int[numRows][numCols];
		
		inverses = new int[this.prime - 1];
		for (int i = 1; i < this.prime; i++) {
			for (int j = 1; j < this.prime; j++) {
				if (i * j % this.prime == 1) {
					inverses[i - 1] = j;
				}
			}
		}
	}
	
	public void setMatrix(int[][] matrix) {
		this.matrix = matrix;
	}
	
	public void setElement(int rowIndex, int colIndex, int element) {
		matrix[rowIndex][colIndex] = element;
	}
	
	public int getElement(int rowIndex, int colIndex) {
		return matrix[rowIndex][colIndex];
	}
	
	public void mod() {
		for (int i = 0; i < this.numRows; i++) {
			for (int j = 0; j < numCols; j++) {
				while (this.matrix[i][j] < 0) {
					this.matrix[i][j] += this.prime;
				}
				this.matrix[i][j] %= this.prime;
			}
		}
	}
	
	public int getInverse(int num) {
		return inverses[num - 1];
	}
	
	public boolean isAllZeroBelowInclusive(int rowIndex, int colIndex) {
		boolean allZero = true;
		for (int i = rowIndex; i < numRows; i++) {
			if (this.getElement(i, colIndex) != 0) {
				allZero = false;
			}
		}
		return allZero;
	}
	
	public void clearColumnBelowAndMakeOne(int rowIndex, int colIndex) {
		this.divideRowBy(rowIndex, this.getElement(rowIndex, colIndex));
		for (int i = rowIndex + 1; i < numRows; i++) {
			this.subtractRowFromRow(this.getElement(i, colIndex), rowIndex, i);
		}
	}
	
	public void swapRows(int rowIndexOne, int rowIndexTwo) {
		for (int j = 0; j < numCols; j++) {
			int temp = matrix[rowIndexOne][j];
			matrix[rowIndexOne][j] = matrix[rowIndexTwo][j];
			matrix[rowIndexTwo][j] = temp;
		}
	}
	
	public void divideRowBy(int rowIndex, int scalar) {
		if (0 < scalar && scalar < this.prime) {
			for (int j = 0; j < numCols; j++) {
				matrix[rowIndex][j] *= getInverse(scalar);
			}
			mod();
		} else {
			System.out.println("Divisor not in GF(p)\\{0}");
		}
	}
	
	public void subtractRowFromRow(int multiple, int rowIndexOne, int rowIndexTwo) {
		for (int j = 0; j < numCols; j++) {
			matrix[rowIndexTwo][j] -= multiple * matrix[rowIndexOne][j];
		}
		mod();
	}
	
	public int getRowIndexOfFirstNonZeroElementBelowInclusive(int rowIndex, int colIndex) {
		for (int i = rowIndex; i < numRows; i++) {
			if (matrix[i][colIndex] != 0) {
				return i;
			}
		}
		System.out.println("All entries in column are zero.");
		return -1;
	}
	
	public MatrixOverGF clone() {
		MatrixOverGF cloned = new MatrixOverGF(numRows, numCols, prime);
		for (int i = 0; i < numRows; i++) {
			for (int j = 0; j < numCols; j++) {
				cloned.setElement(i, j, this.getElement(i, j));
			}
		}
		return cloned;
	}
	
	public MatrixOverGF getRowEchelonForm() {
		MatrixOverGF rowEchelonForm = this.clone();
	    int i = 0;
	    for (int j = 0; j < numCols; j++) {
	        if (rowEchelonForm.isAllZeroBelowInclusive(i, j)) {
	        	// DO NOTHING
	        } else {
	            int swappingIndex = rowEchelonForm.getRowIndexOfFirstNonZeroElementBelowInclusive(i, j);
	            rowEchelonForm.swapRows(swappingIndex, i);
	            if (i < numRows) {
	            	rowEchelonForm.clearColumnBelowAndMakeOne(i, j);
	                i++;
	            }
	        }
	    }
	    return rowEchelonForm;
	}
	
	public MatrixOverGF getColumnEchelonForm() {
		return this.transpose().getRowEchelonForm().transpose();
	}
	
	public void print() {
		String matrixString = "";
		for (int i = 0; i < numRows; i++) {
			matrixString += "(";
			for (int j = 0; j < numCols; j++) {
				matrixString += " " + matrix[i][j] + " ";
			}
			matrixString += ")\n";
		}
		System.out.println(matrixString);
	}
	
	public MatrixOverGF transpose() {
		MatrixOverGF transposed = new MatrixOverGF(numCols, numRows, prime);
		for (int i = 0; i < numRows; i++) {
			for (int j = 0; j < numCols; j++) {
				transposed.setElement(j, i, this.getElement(i, j));
			}
		}
		return transposed;
	}
	
	public MatrixOverGF augmentOnRightBy(MatrixOverGF otherMatrix) {
		if (numRows != otherMatrix.numRows) {
			System.out.println("Size mismatch.");
			return null;
		}
		
		MatrixOverGF augmented = new MatrixOverGF(numRows, numCols + otherMatrix.numCols, this.prime);
		for (int i = 0; i < numRows; i++) {
			for (int j = 0; j < numCols; j++) {
				augmented.setElement(i, j, this.getElement(i, j));
			}
			for (int j = 0; j < otherMatrix.numCols; j++) {
				augmented.setElement(i, numCols + j, otherMatrix.getElement(i, j));
			}
		}
		return augmented;
	}
	
	public MatrixOverGF augmentBelowBy(MatrixOverGF otherMatrix) {
		MatrixOverGF augmented = this.transpose().augmentOnRightBy(otherMatrix.transpose());
		return augmented.transpose();	
	}
	
	public static MatrixOverGF getIdentity(int size) {
		MatrixOverGF identity = new MatrixOverGF(size, size, 1);
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				if (i == j) {
					identity.setElement(i, j, 1);
				} else {
					identity.setElement(i, j, 0);
				}
			}
		}
		return identity;
	}
	
	public int getRank() {
		MatrixOverGF echelonForm = this.getRowEchelonForm();
		int rank = 0;
		for (int i = 0; i < numRows; i++) {
			for (int j = 0; j < numCols; j++) {
				if (echelonForm.getElement(i, j) != 0) {
					rank += 1;
					break;
				}
			}
		}
		return rank;
	}
	
	public ArrayList<Integer> getIndicesOfPivotColumns() {
		ArrayList<Integer> indices = new ArrayList<Integer>();
		for (int i = 0; i < numRows; i++) {
			for (int j = 0; j < numCols; j++) {
				if (this.getElement(i, j) != 0) {
					indices.add(j);
					break;
				}
			}
		}
		return indices;
	}
	
	public boolean zeroColumn(int index) {
		boolean bool = true;
		for (int i = 0; i < this.numRows; i++) {
			if (this.getElement(i, index) != 0) {
				bool = false;
			}
		}
		return bool;
	}
	
	public MatrixOverGF removeZeroColumns() {
		MatrixOverGF matrix = this.clone();
		int num = 0;
		ArrayList<Integer> indices = new ArrayList<Integer>();
		for (int j = 0; j < matrix.numCols; j++) {
			if (matrix.zeroColumn(j)) {
				indices.add(j - num);
				num++;
			}
		}
		for (int index : indices) {
			matrix = matrix.deleteColumn(index);
		}
		return matrix;
	}
	
	public MatrixOverGF deleteColumn(int colIndex) {
		MatrixOverGF modified = new MatrixOverGF(numRows, numCols - 1, this.prime);
		for (int i = 0; i < numRows; i++) {
			for (int j = 0; j < colIndex; j++) {
				modified.setElement(i, j, this.getElement(i, j));
			}
			for (int j = colIndex + 1; j < numCols; j++) {
				modified.setElement(i, j - 1, this.getElement(i, j));
			}
		}
		return modified;
	}
	
	public MatrixOverGF deleteRow(int rowIndex) {
		MatrixOverGF modified = new MatrixOverGF(numRows - 1, numCols, this.prime);
		for (int j = 0; j < numCols; j++) {
			for (int i = 0; i < rowIndex; i++) {
				modified.setElement(i, j, this.getElement(i, j));
			}
			for (int i = rowIndex + 1; i < numRows; i++) {
				modified.setElement(i - 1, j, this.getElement(i, j));
			}
		}
		return modified;
	}
	
	public MatrixOverGF getEquation(int index) {
		MatrixOverGF equation = this.getRowEchelonForm();
		MatrixOverGF constants = new MatrixOverGF(numRows, 1, this.prime);
		for (int i = 0; i < numRows; i++) {
			constants.setElement(i, 0, (this.prime - equation.getElement(i, index)) % this.prime);
		}
		ArrayList<Integer> indices = equation.getIndicesOfPivotColumns();
		for (int j = 0; j < numCols; j++) {
			if (!indices.contains(j)) {
				for (int i = 0; i < numRows; i++) {
					equation.setElement(i, j, 0);
				}
			}
		}
		equation = equation.removeZeroColumns();
		equation = equation.augmentOnRightBy(constants);
		while (equation.numRows > equation.getRank()) {
			equation = equation.deleteRow(equation.numRows - 1);
		}
		return equation;
	}
	
	public MatrixOverGF getBasisElement(int index) {
		ArrayList<Integer> indices = this.getRowEchelonForm().getIndicesOfPivotColumns();
		MatrixOverGF basisElement = new MatrixOverGF(this.numCols, 1, this.prime);
		for (int i = 0; i < basisElement.numRows; i++) {
			if (indices.contains(i)) {
				basisElement.setElement(i, 0, -1);
			} else {
				basisElement.setElement(i, 0, 0);
			}
		}
		basisElement.setElement(index, 0, 1);
		MatrixOverGF equation = this.getEquation(index);
		int[] pivotSolutions = new int[equation.numRows];
		for (int i = equation.numRows - 1 ; i >= 0; i--) {
			pivotSolutions[i] = equation.getElement(i, equation.numCols - 1);
			for (int j = i + 1; j < equation.numCols - 1; j++) {
				pivotSolutions[i] -= equation.getElement(i, j) * pivotSolutions[j];
			}
		}
		int remaining = pivotSolutions.length;
		int insertionIndex = basisElement.numRows - 1;
		while (remaining > 0) {
			if (basisElement.getElement(insertionIndex, 0) == -1) {
				basisElement.setElement(insertionIndex, 0, pivotSolutions[remaining - 1]);
				remaining--;
			}
			insertionIndex--;
		}
		basisElement.mod();
		return basisElement;
	}
	
	public ArrayList<MatrixOverGF> getBaisisOfKernel(boolean print) {
		if (print) {
			System.out.println("<------------original matrix------------->" + " GF(" + prime + ")");
			this.print();
			System.out.println("<------------row echelon form------------>");
			this.getRowEchelonForm().print();
		}
		ArrayList<Integer> indices = this.getRowEchelonForm().getIndicesOfPivotColumns();
		ArrayList<MatrixOverGF> basis = new ArrayList<MatrixOverGF>();
		for (int i = 0; i < numCols; i++) {
			if (!indices.contains(i)) {
				basis.add(this.getBasisElement(i));
				if (print) {
					System.out.println("<-- reduced matrix, basis element pair -->");
					this.getEquation(i).print();
					this.getBasisElement(i).print();
				}
			}
		}
		if (print) {
			System.out.println("<---------------------------------------->" + " GF(" + prime + ")");
		}
		return basis;
	}
	
	public ArrayList<MatrixOverGF> getBasisOfRowSpace(boolean print) {
		MatrixOverGF rowEchelonForm = this.getRowEchelonForm();
		if (print) {
			System.out.println("<------------original matrix------------->" + " GF(" + prime + ")");
			this.print();
			System.out.println("<------------row echelon form------------>");
			rowEchelonForm.print();
		}
		ArrayList<MatrixOverGF> basis = new ArrayList<MatrixOverGF>();
		for (int i = 0; i < numRows; i++) {
			MatrixOverGF basisElement = new MatrixOverGF(1, numCols, prime);
			for (int j = 0; j < numCols; j++) {
				basisElement.setElement(0, j, rowEchelonForm.getElement(i, j));
			}
			if (!basisElement.transpose().isAllZeroBelowInclusive(0, 0)) {
				basis.add(basisElement);
				if (print) {
					System.out.println("<------------row space element----------->");
					basisElement.print();
				}
			}
		}
		if (print) {
			System.out.println("<---------------------------------------->" + " GF(" + prime + ")");
		}
		return basis;
	}
	
}
